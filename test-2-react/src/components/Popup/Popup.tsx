import React from 'react'
import styles from "./Popup.module.css"

interface popupData {
    type: string,
    show: boolean
}

interface IModalData {
    type: string,
    closeModalHandler(data: popupData): void
}

const Popup = (props: IModalData) => {
    return (
        <div className={styles.backdrop}>
            <div className={styles.wrapper}>
                <h2>
                    {props.type === "success" ? "Success!": "Error!"}
                </h2>

                <button
                    onClick={() => props.closeModalHandler({
                        type: props.type,
                        show: false
                    })}
                    className="btn w-52 mt-4 btn-primary mt-auto"
                >
                    {"<"}
                </button>
            </div>
        </div>
    )
}

export default Popup