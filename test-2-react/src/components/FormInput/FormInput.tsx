interface IFormInput {
    email?: string,
    setEmailHandler?(inputValue: string): void
}

function FormInput(props: IFormInput) {
    return (
        <label className="form-control">
            <div className="label">
                <span className="label-text">Email</span>
            </div>
            <input
                value={props.email}
                onChange={e => props.setEmailHandler?.(e.target.value)}
                type="text"
                placeholder="Type here"
                className="input"
            />
        </label>
    )
}

export default FormInput