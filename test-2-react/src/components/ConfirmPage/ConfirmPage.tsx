import { Route, Switch } from 'react-router'
import FormInput from '../FormInput/FormInput.tsx'
import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Popup from '../Popup/Popup.tsx'

function ConfirmPage() {
    const [email, setEmail] = useState("")
    const [popupData, setPopupData] = useState({
        show: false,
        type: ""
    })

    useEffect(() => {
        const data = sessionStorage.getItem("email")

        if(data !== null) {
            setEmail(data)
        }
    }, [])

    const toPrevPage = () => {
        history.back()
    }

    const sendDataHandler = async () => {
        try {
            await axios.post("/api/endpoint", {email: email})

            setPopupData({
                show: true,
                type: "success"
            })
        } catch (e) {
            setPopupData({
                show: true,
                type: "error"
            })
        }
    }

    return(
        <Switch>
            <Route>
                <FormInput email={email}/>
                <div className="p-1"></div>

                <div className="flex justify-between mt-auto">
                    <button onClick={toPrevPage} className="btn w-40 btn-neutral mt-auto">Back</button>
                    <button onClick={sendDataHandler} className="btn w-40 btn-primary mt-auto">Confirm</button>
                </div>

                {
                    popupData.show ?
                    <Popup type={popupData.type} closeModalHandler={setPopupData} /> : null
                }
            </Route>
        </Switch>
    )
}

export default ConfirmPage