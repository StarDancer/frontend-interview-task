interface IFormCheckbox {
    checked: boolean,
    setCheckboxHandler(inputChecked: boolean): void
}

function FormCheckbox(props: IFormCheckbox) {
    return (
        <div className="form-control">
            <label className="label cursor-pointer justify-start gap-2">
                <input
                    checked={props.checked}
                    onChange={e => props.setCheckboxHandler(e.target.checked)}
                    type="checkbox"
                    className="checkbox checkbox-primary"
                />
                <span className="label-text">I agree</span>
            </label>
        </div>
    )
}

export default FormCheckbox