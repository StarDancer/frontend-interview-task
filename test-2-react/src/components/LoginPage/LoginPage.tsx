import React, { useEffect, useState } from 'react'
import { Route, Switch, withRouter,  } from 'react-router'
import FormInput from '../FormInput/FormInput.tsx'
import FormCheckbox from '../FormCheckbox/FormCheckbox.tsx'

let timeout: any;

const LoginPage = withRouter(({history}) => {
    const [email, setEmail] = useState("")
    const [checkbox, setCheckbox] = useState(false)
    const [validEmail, setValidEmail] = useState(false)

    useEffect(() => {
        const data = sessionStorage.getItem("email")

        if(data !== null && checkEmail(data)) {
            setValidEmail(true)
            setEmail(data)
        }
    }, [])

    const checkEmail = (email: string): boolean => {
        const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/

        if(emailRegex.test(email)) {
            return true
        }

        return false
    }

    const setEmailHandler = (inputValue: string) => {
        if(checkEmail(inputValue)) {
            setValidEmail(true)
            sessionStorage.setItem("email", inputValue)
        } else {
            setValidEmail(false)
            sessionStorage.removeItem("email")
        }

        setEmail(inputValue)
    }

    const holdButtonHandler = () => {
        timeout = setTimeout(() => {
            history.push("/login/step-2")
        }, 500)
    }

    const releaseButtonHandler = () => {
        clearTimeout(timeout)
    }

    return (
        <Switch>
            <Route>
                <FormInput email={email} setEmailHandler={setEmailHandler}/>
                <div className="p-1"></div>
                <FormCheckbox checked={checkbox} setCheckboxHandler={setCheckbox}/>

                <button
                    onMouseDown={holdButtonHandler}
                    onMouseUp={releaseButtonHandler}
                    disabled={!(checkbox && validEmail)}
                    className="btn btn-primary mt-auto"
                >
                    Hold to proceed
                </button>
            </Route>
        </Switch>
    )
})

export default LoginPage